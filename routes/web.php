<?php

use App\Mail\FormShipped;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::post('/form', function (Request $request) {
    Mail::to(config('mail.to'))->send(new FormShipped($request->validate([
        'name' => 'required',
        'email' => 'required|email:rfc',
        'wtp' => 'string',
        'message' => 'string',
    ])));
})->name('form');
