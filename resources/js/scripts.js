const jQuery = require('jquery');

let menuTrigger = document.querySelector(".menu-trigger");
let menu = document.querySelector(".nav");
let menuItem = document.querySelectorAll(".nav__list a");

menuTrigger.addEventListener('click', function () {
    this.classList.toggle('active');
    menu.classList.toggle('active')
});


menuItem.forEach(function (item) {
    item.addEventListener('click', function () {
        menu.classList.remove('active');
        menuTrigger.classList.remove('active');
    });
})


/*forms */

jQuery('.form__body').on('submit', function (event) {
    event.preventDefault();
    jQuery.ajax({
        type: 'POST',
        url: '/form',
        data: jQuery('.form__body').serialize(),
        beforeSend: function () {
            jQuery("#error").fadeOut();
        },
        success: function (response) {
            jQuery('.form').addClass('form--sending');
            jQuery('.connect__result').removeClass('hidden');
        }
    });
});
