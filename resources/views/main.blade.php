<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover">
    <meta name="robots" content="all">
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:description" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <!-- <link rel="icon" href="img/favicon.png" type="image/png" sizes="34x34">
   <link rel="icon" href="img/favicon.ico" type="image/x-icon"> -->
    <meta name="theme-color" content="#083dcc">

    <title></title>
    <link rel="preload" href="/fonts/Gilroy-Regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="stylesheet" href="/css/main.css">

</head>

<body>

<!--[if lt IE 9]>
<p class="browsehappy">Ваш браузер <strong>устарел</strong>. Пожалуйста<a href="http://browsehappy.com/">обновите</a>
<![endif]-->

<div class="wrapper">
    <header class="header">
        <div class="container row">
            <div class="logo"><img src="/img/logo.svg"></div>
            <nav class="nav">
                <ul class="nav__list">
                    <li class="nav__item"><a href="">About Us</a></li>
                    <li class="nav__item"><a href="">Services</a></li>
                    <li class="nav__item"><a href="">Testimonials</a></li>
                    <li class="nav__item current"><a href="">Contact</a></li>
                </ul>
            </nav>
            <a href="" class="btn">Send us a message</a>
            <button class="menu-trigger" type="button"></button>
        </div>
    </header>
    <section class="section connect" id="contact">
        <div class="container">
            <h1 class="section__title section__title--funsy">Let's connect</h1>
            <div class="connect__row">
                <div class="connect__form">
                    <!-- TODO по факту отправки письма накидывать .form--sending -->
                    <div class="form">
                        <div class="form__title">Send us a message</div>
                        <form action="{{ route('form') }}" method="post" name="connect_form" class="form__body">
                            @csrf
                            <div class="form__row">
                                <input type="text" name="name" placeholder="NAME" class="form__input" id="name" required>
                                <label for="name" class="form__label">NAME</label>
                            </div>
                            <div class="form__row">
                                <input type="email" name="email" placeholder="EMAIL" class="form__input" id="email" required>
                                <label for="email" class="form__label">EMAIL</label>
                            </div>
                            <div class="form__row">
                                <input type="tel" name="wtp" placeholder="WHATSAPP NO" class="form__input" id="wtp">
                                <label for="wtp" class="form__label">WHATSAPP NO</label>
                            </div>
                            <div class="form__row">
                                <textarea name="message" placeholder="YOUR MESSAGE" class="form__input" id="message"></textarea>
                                <label for="message" class="form__label">YOUR MESSAGE</label>
                            </div>
                            <div class="form__row">
                                <button class="btn">Send</button>
                            </div>
                        </form>
                    </div>
                    <!-- TODO по факту отправки письма убирать .hidden -->
                    <div class="connect__result hidden">Your message has been sent</div>
                </div>
                <div class="logo-vertical"><img src="/img/logo_v.svg"></div>
            </div>
        </div>
    </section>
    <section class="section offer">
        <div class="container">
            <h1 class="section__title">Get your free strategy<br>session now</h1>
            <div class="section__wrapper" style="background-image: url(/img/hero.jpg);">
                <div class="section__content center">
                    <p>Book a time to discuss your marketing objectives and your budget and our team will create a strategy that's perfect for your business growth. </p>
                    <p>We can’t wait to partner with you!</p>
                    <p><a href="#contact" class="btn">Contact Us</a></p>
                </div>
            </div>
        </div>
    </section>
</div>
<footer class="footer">
    <div class="container">
        <div class="footer__logo"><img src="/img/logo_h.svg" alt=""></div>
        <div class="footer__content">Copyright © 2021 9Pro Digital Agency | Powered by 9Pro Digital Agency | Company Number 13525375 RODING HOUSE 2 CAMBRIDGE ROAD UNIT 13 BARKING ENGLAND IG11 8NL</div>
    </div>
</footer>
<script src="/js/scripts.js"></script>
</body>
</html>
