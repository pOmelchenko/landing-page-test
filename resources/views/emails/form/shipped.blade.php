@component('mail::message')
Name: {{ $fields['name'] }}<br>
Email: {{ $fields['email'] }}<br>
Whatsapp No: {{ $fields['wtp'] }}<br>
Message: {{ $fields['message'] }}
@endcomponent
